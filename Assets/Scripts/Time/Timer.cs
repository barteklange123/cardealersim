﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;



public class Timer : MonoBehaviour
{

    public float clockSpeed = 1;
    Text text_Timer;
    int hours = 0, minutes = 0;
    string text_hours, text_minutes;
    bool waiting;

    // Use this for initialization
    void Start()
    {
        ///Zrobić ładowanie ostatniego czasu
        text_Timer = gameObject.GetComponent<Text>();
        //StartCoroutine("AddMinute");
    }

    // Update is called once per frame
    void Update()
    {
        if (!waiting)
        {
            StartCoroutine("Wait");
            minutes++;
            SetTimerText();
        }
    }

    IEnumerator Wait()
    {
        waiting = true;
        yield return new WaitForSeconds(clockSpeed);
        waiting = false;

    }

    void SetTimerText()
    {
        if (minutes >= 60)
        {
            hours++;
            minutes = 0;
        }

        if (hours >= 24)
        {
            hours = 0;
        }

        text_hours = hours.ToString("00");
        text_minutes = minutes.ToString("00");

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.Append(text_hours);
        stringBuilder.Append(":");
        stringBuilder.Append(text_minutes);

        text_Timer.text = stringBuilder.ToString();
    }
}
