﻿
public class ValueOutOfRangeException : System.Exception
{
    public ValueOutOfRangeException(string message) : base(message)
    {

    }
}
