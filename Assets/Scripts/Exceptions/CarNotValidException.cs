﻿
public class CarNotValidException : System.Exception
{
    public CarNotValidException(string message) : base(message)
    {

    }
}
