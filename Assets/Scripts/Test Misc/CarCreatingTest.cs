﻿using NUnit.Framework;
using UnityEngine;

public abstract class CarCreatingTest
{
    [Test]
    public abstract void GetBarnFind();
    [Test]
    public abstract void GetRandom();
    [Test]
    public abstract void GetRandomNotStolen();
    [Test]
    public abstract void GetRandomStolen();



    protected void CheckCar(Car car)
    {

        if (car.Engine == null)
        {
            Debug.Log("Engine has no value");
        }
        if (car.Suspension == null)
        {
            Debug.Log("Suspension has no value");
        }
        if (car.Gearbox == null)
        {
            Debug.Log("Gearbox has no value");
        }
        if (car.Body == null)
        {
            Debug.Log("Body has no value");
        }
        if (car.Abs == null)
        {
            Debug.Log("Abs has no value");
        }
        if (car.Airbags == null)
        {
            Debug.Log("Airbags has no value");
        }
        if (car.Radio == null)
        {
            Debug.Log("Radio has no value");
        }
        if (car.Esp == null)
        {
            Debug.Log("Esp has no value");
        }
        if (car.LeatherSeats == null)
        {
            Debug.Log("LeatherSeats has no value");
        }
        if (car.Ac == null)
        {
            Debug.Log("Ac has no value");
        }
        if (car.ParkingSensors == null)
        {
            Debug.Log("ParkingSensors has no value");
        }
        if (car.Xenon == null)
        {
            Debug.Log("Xenon has no value");
        }
        if (car.SunRoof == null)
        {
            Debug.Log("SurRoof has no value");
        }
        if (car.CruiseControl == null)
        {
            Debug.Log("CruiseControl has no value");
        }
    }
}
