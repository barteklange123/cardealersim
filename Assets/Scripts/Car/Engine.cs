﻿using System;
using UnityEngine;

public enum EngineType { PETROL, DIESEL, HYBRYD, ELECTRIC };

/// <summary>
/// Class representing Engine
/// </summary>
[Serializable]
public class Engine : CarPart
{
    [SerializeField]
    string engineName;
    [SerializeField]
    EngineType engineType;
    [SerializeField]
    int rarityBonus;
    [SerializeField]
    int priceBonus;

    /// <summary>
    /// Create engine.
    /// </summary>
    /// <param name="condition">Ranges from 1 to 5.</param>
    /// <param name="difficulty">Ranges from 0 to 100.</param>
    /// <param name="engineName">Name of the engine</param>
    /// <param name="engineType">Type, ex. petrol, diesel, electric, hybrid</param>
    /// <param name="rarityBonus">Bonus to rarity.</param>
    /// <param name="priceBonus">Bonus to price.</param>
    public Engine(int condition, int difficulty, string engineName, EngineType engineType, int rarityBonus, int priceBonus)
        : base(condition, difficulty, priceBonus)
    {
        this.EngineName = engineName;
        this.EngineType = engineType;
        this.RarityBonus = rarityBonus;
        this.PriceBonus = priceBonus;
    }

    public string EngineName
    {
        get
        {
            return engineName;
        }

        set
        {
            engineName = value;
        }
    }

    public EngineType EngineType
    {
        get
        {
            return engineType;
        }

        set
        {
            engineType = value;
        }
    }

    public int RarityBonus
    {
        get
        {
            return rarityBonus;
        }

        set
        {
            if (value >= 0 && value <= 100)
                rarityBonus = value;
            else
            {
                throw new ValueOutOfRangeException("Value must be in range [0...100]");
            }
        }
    }

    public int PriceBonus
    {
        get
        {
            return priceBonus;
        }

        set
        {
            if (value >= 0)
                priceBonus = value;
            else
            {
                throw new ValueOutOfRangeException("Value cannot be lower than 0");
            }
        }
    }
}
