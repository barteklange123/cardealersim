﻿using System;
using UnityEngine;

public enum Condition { CANT, NO, YES, BROKEN, RANDOM }

/// <summary>
/// Class represents equipment, ex. ABS, Radio...
/// WAŻNE: TRZEBA SPRAWDZIĆ DZIAŁANIE CONDITION.RANDOM!!!
/// </summary>
[Serializable]
public class Equipment
{
    [SerializeField]
    Condition condition;
    [SerializeField]
    int rarityBonus;
    [SerializeField]
    int priceBonus;
    [SerializeField]
    int difficulty; //Jak cieżko naprawić (ile wymaga umiejętności)
    [SerializeField]
    int chance; //szansa na to, ze wystąpi
    [SerializeField]
    int chanceBroken; //szansa na to, ze będzie popsuty, jeżeli wystąpi

    /// <summary>
    /// Create equimpent element.
    /// </summary>
    /// <param name="condition">Yes: Car has this equiment. No: Doesn't have. Broken: Car has, but it is broken. Can't: Car cant have this type of equipment
    /// Random: state will be set using "chance" and "chanceBroken" values</param>
    /// <param name="rarityBonus">Bonus to rarity.</param>
    /// <param name="priceBonus">Bonus to price.</param>
    /// <param name="difficulty">Ranges from 0 to 100.</param>
    /// <param name="chance">Chance that car will have this equipment (Condition.Yes)</param>
    /// <param name="chanceBroken">Chance that car will have this equipment, but broken (Condition.Broken)</param>
    public Equipment(Condition condition, int rarityBonus, int priceBonus, int difficulty, int chance, int chanceBroken)
    {
        this.RarityBonus = rarityBonus;
        this.PriceBonus = priceBonus;
        this.Difficulty = difficulty;
        this.Chance = chance;
        this.ChanceBroken = chanceBroken;
        this.Condition = GetCondition(condition);
    }

    public Condition Condition
    {
        get
        {
            return condition;
        }

        set
        {
            condition = value;
        }

    }

    public int RarityBonus
    {
        get
        {
            return rarityBonus;
        }

        set
        {
            if (value >= 0 && value <= 100)
                rarityBonus = value;
            else
            {
                throw new ValueOutOfRangeException("Value must be in range [0...100]");
            }
        }
    }

    public int PriceBonus
    {
        get
        {
            return priceBonus;
        }

        set
        {
            if (value >= 0)
                priceBonus = value;
            else
            {
                throw new ValueOutOfRangeException("Value cannot be lower than 0");
            }
        }
    }


    public int Difficulty
    {
        get
        {
            return difficulty;
        }

        set
        {
            if (value >= 0 && value <= 100)
                difficulty = value;
            else
            {
                throw new ValueOutOfRangeException("Value must be in range [0...100]");
            }
        }
    }

    public int Chance
    {
        get
        {
            return chance;
        }

        set
        {
            if (value >= 0 && value <= 100)
                chance = value;
            else
            {
                throw new ValueOutOfRangeException("Value must be in range [0...100]");
            }
        }
    }

    public int ChanceBroken
    {
        get
        {
            return chanceBroken;
        }

        set
        {
            if (value >= 0 && value <= 100)
                chanceBroken = value;
            else
            {
                throw new ValueOutOfRangeException("Value must be in range [0...100]");
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="condition"></param>
    /// <returns></returns>
    private Condition GetCondition(Condition condition)
    {
        return condition;
    }

    private Condition GetCondition(int chance, int chanceBroken)
    {
        MyRandom random = new MyRandom();

        if (random.GetBoolean(chance)) //if random draws true then car has equipment
        {
            if (random.GetBoolean(chanceBroken)) // if random draws true, this equipment is broken
            {
                return Condition.BROKEN;
            }
            else // if draws false, its good
            {
                return Condition.YES;
            }
        }
        else
        {
            return Condition.NO;
        }

    }
}
