﻿using System;

/// <summary>
/// Error car is a class made for testing game and handling errors.
/// At this state all stats are predefined, thus they are NOT random. At least many of them.
/// </summary>
public class ErrorCar : CarCreator
{
    private enum EngineErrorCar { P16 };
    private enum GearboxErrorCar { MANUAL };

    Random r = new Random();

    protected override void GetCarValues()
    {
        baseprice = 1000;
        rarity = 50;
        minYear = 1995;
        maxYear = 2005;
        name = "Error Car";
        mileage = 100000;
        realMileage = 100000;
        //Body:
        bodyPrice = 4000;
        bodyDifficulty = 40;
        //Suspention:
        suspensionPrice = 3000;
        suspensionDifficulty = 30;
        //ABS:
        absRarityBonus = 20;
        absPrice = 1000;
        absDifficulty = 30;
        absChance = 30;
        //AC:
        acRarityBonus = 20;
        acPrice = 1000;
        acDifficulty = 30;
        acChance = 30;
        //Airbag:
        airbagRarityBonus = 20;
        airbagPrice = 1000;
        airbagDifficulty = 30;
        airbagChance = 30;
        //Cruise Control:
        cruiseRarityBonus = 20;
        cruisePrice = 1000;
        cruiseDifficulty = 30;
        cruiseChance = 30;
        //ESP:
        espRarityBonus = 20;
        espPrice = 1000;
        espDifficulty = 30;
        espChance = 30;
        //Leather Seats:
        leatherRarityBonus = 20;
        leatherPrice = 1000;
        leatherDifficulty = 30;
        leatherChance = 30;
        //Parking sensors:
        parkingRarityBonus = 20;
        parkingPrice = 1000;
        parkingDifficulty = 30;
        parkingChance = 30;
        //Radio:
        radioRarityBonus = 20;
        radioPrice = 1000;
        radioDifficulty = 30;
        radioChance = 30;
        //Sunroof:
        sunroofRarityBonus = 20;
        sunroofPrice = 1000;
        sunroofDifficulty = 30;
        sunroofChance = 30;
        //Xenon:
        xenonRarityBonus = 20;
        xenonPrice = 1000;
        xenonDifficulty = 30;
        xenonChance = 30;
    }

    protected override void SetEngine(int minCondition, int maxCondition, Car _car)
    {
        _car.Engine = new Engine(r.Next(minCondition, maxCondition), 10, "1.6 Petrol", EngineType.PETROL, 0, 5000);
    }

    protected override void SetGearbox(int minCondition, int maxCondition, Car _car)
    {
        _car.Gearbox = new Gearbox(r.Next(minCondition, maxCondition), 20, GearboxType.MANUAL, 0, 3000);
    }
}
