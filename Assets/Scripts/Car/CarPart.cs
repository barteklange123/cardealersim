﻿using System;
using UnityEngine;

[Serializable]
public abstract class CarPart
{
    [SerializeField]
    int condition;
    /*
    [SerializeField]
    int fullRepairCost; //Koszt wymiany
    [SerializeField]
    int goodRepairCost; //Koszt fachowej naprawy
    [SerializeField]
    int badRepairCost; //Koszt niefachowej naprawy
    */
    [SerializeField]
    int difficulty; //Jak cieżko naprawić (ile wymaga umiejętności)
    [SerializeField]
    int priceBonus;

    /// <summary>
    /// Abstract class, that shares fields with Engine, Gearbox, Suspension, Body
    /// </summary>
    /// <param name="condition">Ranges from 1 to 5.</param>
    /// <param name="difficulty">Ranges from 0 to 100.</param>
    /// <param name="priceBonus">Bonus to price</param>
    public CarPart(int condition, int difficulty, int priceBonus)
    {
        Condition = condition;
        Difficulty = difficulty;
        PriceBonus = priceBonus;
    }





    public int Condition
    {
        get
        {
            return condition;
        }

        set
        {
            if (value >= 1 && value <= 5)
                condition = value;
            else
            {
                throw new ValueOutOfRangeException("Value must be in range [1...5]");
            }
        }
    }

    public int Difficulty
    {
        get
        {
            return difficulty;
        }

        set
        {
            if (value >= 0 && value <= 100)
                difficulty = value;
            else
            {
                throw new ValueOutOfRangeException("Value must be in range [0...10]");
            }
        }
    }

    public int PriceBonus
    {
        get
        {
            return priceBonus;
        }

        set
        {
            priceBonus = value;
        }
    }
}
