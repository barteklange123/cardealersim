﻿using System;
using UnityEngine;
/// <summary>
/// Class representing Car
/// </summary>
/// 

[Serializable]
public class Car
{
    [SerializeField]
    private ECarName name;
    [SerializeField]
    private ECarType type;
    [SerializeField]
    private int year;
    [SerializeField]
    private int rarity;
    [SerializeField]
    private int basePrice;
    [SerializeField]
    private int mileage;
    [SerializeField]
    private int realmileage;
    [SerializeField]
    private bool isMileageReal;
    [SerializeField]
    private bool isStolen;
    [SerializeField]
    private Engine engine;
    [SerializeField]
    private Gearbox gearbox;
    [SerializeField]
    private Suspension suspension;
    [SerializeField]
    private Body body;
    [SerializeField]
    private Equipment abs;
    [SerializeField]
    private Equipment airbags;
    [SerializeField]
    private Equipment radio;
    [SerializeField]
    private Equipment esp;
    [SerializeField]
    private Equipment leatherSeats;
    [SerializeField]
    private Equipment ac;
    [SerializeField]
    private Equipment parkingSensors;
    [SerializeField]
    private Equipment xenon;
    [SerializeField]
    private Equipment sunRoof;
    [SerializeField]
    private Equipment cruiseControl;
    [SerializeField]
    private bool isBarnFind;
    [SerializeField]
    private int totalPrice=0;

    public Car() { }

    public int Rarity
    {
        get
        {
            return rarity;
        }

        set
        {
            if (value >= 0)
                rarity = value;
        }
    }

    public int BasePrice
    {
        get
        {
            return basePrice;
        }

        set
        {
            if (value > 0)
                basePrice = value;
        }
    }

    public int Mileage
    {
        get
        {
            return mileage;
        }

        set
        {
            if (value >= 0)
                mileage = value;
        }
    }

    public int Realmileage
    {
        get
        {
            return realmileage;
        }

        set
        {
            if (value >= 0)
                realmileage = value;
        }
    }

    public bool IsStolen
    {
        get
        {
            return isStolen;
        }

        set
        {
            isStolen = value;
        }
    }

    public Engine Engine
    {
        get
        {
            return engine;
        }

        set
        {
            if (value != null)
                engine = value;
        }
    }

    public Gearbox Gearbox
    {
        get
        {
            return gearbox;
        }

        set
        {
            if (value != null)
                gearbox = value;
        }
    }

    public bool IsBarnFind
    {
        get
        {
            return isBarnFind;
        }

        set
        {
            isBarnFind = value;
        }
    }

    public Suspension Suspension
    {
        get
        {
            return suspension;
        }

        set
        {
            if (value != null)
                suspension = value;
        }
    }

    public Body Body
    {
        get
        {
            return body;
        }

        set
        {
            if (value != null)
                body = value;
        }
    }

    public int Year
    {
        get
        {
            return year;
        }

        set
        {
            if (value > 1900 && value < 2100)
                year = value;
            else
            {
                throw new ValueOutOfRangeException("Value must be in [1900; 2100] range");
            }
        }
    }

    public ECarName Name
    {
        get
        {
            return name;
        }

        set
        {
            name = value;
        }
    }

    public ECarType Type
    {
        get
        {
            return type;
        }

        set
        {
            type = value;
        }
    }

    public Equipment Abs
    {
        get
        {
            return abs;
        }

        set
        {
            if (value != null)
                abs = value;
        }
    }

    public Equipment Airbags
    {
        get
        {
            return airbags;
        }

        set
        {
            if (value != null)
                airbags = value;
        }
    }

    public Equipment Radio
    {
        get
        {
            return radio;
        }

        set
        {
            if (value != null)
                radio = value;
        }
    }

    public Equipment Esp
    {
        get
        {
            return esp;
        }

        set
        {
            if (value != null)
                esp = value;
        }
    }

    public Equipment LeatherSeats
    {
        get
        {
            return leatherSeats;
        }

        set
        {
            if (value != null)
                leatherSeats = value;
        }
    }

    public Equipment Ac
    {
        get
        {
            return ac;
        }

        set
        {
            if (value != null)
                ac = value;
        }
    }

    public Equipment ParkingSensors
    {
        get
        {
            return parkingSensors;
        }

        set
        {
            if (value != null)
                parkingSensors = value;
        }
    }

    public Equipment Xenon
    {
        get
        {
            return xenon;
        }

        set
        {
            if (value != null)
                xenon = value;
        }
    }

    public Equipment SunRoof
    {
        get
        {
            return sunRoof;
        }

        set
        {
            if (value != null)
                sunRoof = value;
        }
    }

    public Equipment CruiseControl
    {
        get
        {
            return cruiseControl;
        }

        set
        {
            if (value != null)
                cruiseControl = value;
        }
    }

    public bool IsMileageReal
    {
        get
        {
            return isMileageReal;
        }

        set
        {
            isMileageReal = value;
        }
    }

    public int TotalPrice
    {
        get
        {
            return totalPrice;
        }

        set
        {
            if (value > 0)
                totalPrice = value;
            else
            {
                throw new ValueOutOfRangeException("value must be greater than 0");
            }
        }
    }

    /// <summary>
    /// Method checks every field of car. If any is null, method returns false.
    /// Use this method to prevent using not fully created Car.
    /// </summary>
    /// <returns>True if every field has value. False if even one field has no value.</returns>
    public bool IsValid()
    {
        if (engine != null
            && suspension != null
            && gearbox != null
            && body != null
            && Abs != null
            && Airbags != null
            && Radio != null
            && Esp != null
            && LeatherSeats != null
            && Ac != null
            && ParkingSensors != null
            && Xenon != null
            && SunRoof != null
            && CruiseControl != null)
            return true;
        else
            return false;
    }

    public virtual void SetEngine(int minCondition, int maxCondition) { }
    public virtual void SetGearbox(int minCondition, int maxCondition) { }

}


