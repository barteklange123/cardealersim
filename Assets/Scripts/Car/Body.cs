﻿using System;
using UnityEngine;

/// <summary>
/// Class represents body and chassis of car.
/// </summary>
[Serializable]
public class Body : CarPart
{
    [SerializeField]
    private bool hasRust;

    /// <summary>
    /// Create body.
    /// </summary>
    /// <param name="condition">Ranges from 1 to 5.</param>
    /// <param name="difficulty">Ranges from 0 to 100.</param>
    /// <param name="hasRust">Car has rust. It needs fullRepair</param>
    public Body(int condition, int difficulty, int priceBonus, bool hasRust) : base(condition, difficulty, priceBonus)
    {
        HasRust = hasRust;
    }

    public bool HasRust
    {
        get
        {
            return hasRust;
        }

        set
        {
            hasRust = value;
        }
    }
}
