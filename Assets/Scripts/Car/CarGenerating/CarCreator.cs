﻿using System;

public class CarCreator
{
    Car car = new Car();
    Random r = new Random();

    public int baseprice;
    public int rarity;
    public int minYear;
    public int maxYear;
    public string name;
    public int mileage;
    public int realMileage;

    //Body:
    public int bodyPrice;
    public int bodyDifficulty;
    //Suspention:
    public int suspensionPrice;
    public int suspensionDifficulty;

    //ABS:
    public int absRarityBonus;
    public int absPrice;
    public int absDifficulty;
    public int absChance;
    //AC:
    public int acRarityBonus;
    public int acPrice;
    public int acDifficulty;
    public int acChance;
    //Airbag:
    public int airbagRarityBonus;
    public int airbagPrice;
    public int airbagDifficulty;
    public int airbagChance;
    //Cruise Control:
    public int cruiseRarityBonus;
    public int cruisePrice;
    public int cruiseDifficulty;
    public int cruiseChance;
    //ESP:
    public int espRarityBonus;
    public int espPrice;
    public int espDifficulty;
    public int espChance;
    //Leather Seats:
    public int leatherRarityBonus;
    public int leatherPrice;
    public int leatherDifficulty;
    public int leatherChance;
    //Parking sensors:
    public int parkingRarityBonus;
    public int parkingPrice;
    public int parkingDifficulty;
    public int parkingChance;
    //Radio:
    public int radioRarityBonus;
    public int radioPrice;
    public int radioDifficulty;
    public int radioChance;
    //Sunroof:
    public int sunroofRarityBonus;
    public int sunroofPrice;
    public int sunroofDifficulty;
    public int sunroofChance;
    //Xenon:
    public int xenonRarityBonus;
    public int xenonPrice;
    public int xenonDifficulty;
    public int xenonChance;

    protected virtual void GetCarValues()
    {
    }

    /// <summary>
    /// Creates car with random statistics.
    /// </summary>
    /// <returns></returns>
    public Car GetRandom()
    {
        GetCarValues();
        car.Abs = new Equipment(Condition.BROKEN, absRarityBonus, absPrice, absDifficulty, chance: absChance, chanceBroken: 50);
        car.Ac = new Equipment(Condition.BROKEN, acRarityBonus, acPrice, acDifficulty, acChance, chanceBroken: 50);
        car.Airbags = new Equipment(Condition.BROKEN, airbagRarityBonus, airbagPrice, airbagDifficulty, airbagChance, chanceBroken: 50);
        car.BasePrice = baseprice;
        car.Body = new Body(r.Next(1, 3), bodyDifficulty, bodyPrice, true);
        car.CruiseControl = new Equipment(Condition.BROKEN, cruiseRarityBonus, cruisePrice, cruiseDifficulty, cruiseChance, chanceBroken: 50);
        car.Esp = new Equipment(Condition.BROKEN, espRarityBonus, espPrice, espDifficulty, espChance, chanceBroken: 50);
        car.IsBarnFind = true;
        car.IsMileageReal = true;
        car.IsStolen = false;
        car.LeatherSeats = new Equipment(Condition.BROKEN, leatherRarityBonus, leatherPrice, leatherDifficulty, leatherChance, chanceBroken: 50);
        car.Mileage = mileage;
        car.Name = ECarName.Error_Car;
        car.ParkingSensors = new Equipment(Condition.BROKEN, parkingRarityBonus, parkingPrice, parkingDifficulty, parkingChance, chanceBroken: 50);
        car.Radio = new Equipment(Condition.BROKEN, radioRarityBonus, radioPrice, radioDifficulty, radioChance, chanceBroken: 50);
        car.Rarity = rarity;
        car.Realmileage = realMileage;
        car.SunRoof = new Equipment(Condition.BROKEN, sunroofRarityBonus, sunroofPrice, sunroofDifficulty, sunroofChance, chanceBroken: 50);
        car.Suspension = new Suspension(r.Next(1, 3), suspensionDifficulty, suspensionPrice);
        car.Type = ECarType.CITY;
        car.Xenon = new Equipment(Condition.BROKEN, xenonRarityBonus, xenonPrice, xenonDifficulty, xenonChance, chanceBroken: 50);
        car.Year = r.Next(minYear, maxYear);
        SetEngine(1, 3, car);
        SetGearbox(1, 3, car);

        return car;
    }



    /// <summary>
    /// Creates car with random statistics, but isStolen=false.
    /// </summary>
    /// <returns></returns>
    public Car GetRandomNotStolen()
    {
        GetCarValues();
        car.Abs = new Equipment(Condition.BROKEN, absRarityBonus, absPrice, absDifficulty, chance: absChance, chanceBroken: 50);
        car.Ac = new Equipment(Condition.BROKEN, acRarityBonus, acPrice, acDifficulty, acChance, chanceBroken: 50);
        car.Airbags = new Equipment(Condition.BROKEN, airbagRarityBonus, airbagPrice, airbagDifficulty, airbagChance, chanceBroken: 50);
        car.BasePrice = baseprice;
        car.Body = new Body(r.Next(1, 3), bodyDifficulty, bodyPrice, true);
        car.CruiseControl = new Equipment(Condition.BROKEN, cruiseRarityBonus, cruisePrice, cruiseDifficulty, cruiseChance, chanceBroken: 50);
        car.Esp = new Equipment(Condition.BROKEN, espRarityBonus, espPrice, espDifficulty, espChance, chanceBroken: 50);
        car.IsBarnFind = true;
        car.IsMileageReal = true;
        car.IsStolen = false;
        car.LeatherSeats = new Equipment(Condition.BROKEN, leatherRarityBonus, leatherPrice, leatherDifficulty, leatherChance, chanceBroken: 50);
        car.Mileage = mileage;
        car.Name = ECarName.Error_Car;
        car.ParkingSensors = new Equipment(Condition.BROKEN, parkingRarityBonus, parkingPrice, parkingDifficulty, parkingChance, chanceBroken: 50);
        car.Radio = new Equipment(Condition.BROKEN, radioRarityBonus, radioPrice, radioDifficulty, radioChance, chanceBroken: 50);
        car.Rarity = rarity;
        car.Realmileage = realMileage;
        car.SunRoof = new Equipment(Condition.BROKEN, sunroofRarityBonus, sunroofPrice, sunroofDifficulty, sunroofChance, chanceBroken: 50);
        car.Suspension = new Suspension(r.Next(1, 3), suspensionDifficulty, suspensionPrice);
        car.Type = ECarType.CITY;
        car.Xenon = new Equipment(Condition.BROKEN, xenonRarityBonus, xenonPrice, xenonDifficulty, xenonChance, chanceBroken: 50);
        car.Year = r.Next(minYear, maxYear);
        SetEngine(1, 3, car);
        SetGearbox(1, 3, car);

        return car;
    }




    /// <summary>
    /// Creates car with random statistics, but isStolen=true
    /// </summary>
    /// <returns></returns>
    public Car GetRandomStolen()
    {
        GetCarValues();
        car.Abs = new Equipment(Condition.BROKEN, absRarityBonus, absPrice, absDifficulty, chance: absChance, chanceBroken: 50);
        car.Ac = new Equipment(Condition.BROKEN, acRarityBonus, acPrice, acDifficulty, acChance, chanceBroken: 50);
        car.Airbags = new Equipment(Condition.BROKEN, airbagRarityBonus, airbagPrice, airbagDifficulty, airbagChance, chanceBroken: 50);
        car.BasePrice = baseprice;
        car.Body = new Body(r.Next(1, 3), bodyDifficulty, bodyPrice, true);
        car.CruiseControl = new Equipment(Condition.BROKEN, cruiseRarityBonus, cruisePrice, cruiseDifficulty, cruiseChance, chanceBroken: 50);
        car.Esp = new Equipment(Condition.BROKEN, espRarityBonus, espPrice, espDifficulty, espChance, chanceBroken: 50);
        car.IsBarnFind = true;
        car.IsMileageReal = true;
        car.IsStolen = true;
        car.LeatherSeats = new Equipment(Condition.BROKEN, leatherRarityBonus, leatherPrice, leatherDifficulty, leatherChance, chanceBroken: 50);
        car.Mileage = mileage;
        car.Name = ECarName.Error_Car;
        car.ParkingSensors = new Equipment(Condition.BROKEN, parkingRarityBonus, parkingPrice, parkingDifficulty, parkingChance, chanceBroken: 50);
        car.Radio = new Equipment(Condition.BROKEN, radioRarityBonus, radioPrice, radioDifficulty, radioChance, chanceBroken: 50);
        car.Rarity = rarity;
        car.Realmileage = realMileage;
        car.SunRoof = new Equipment(Condition.BROKEN, sunroofRarityBonus, sunroofPrice, sunroofDifficulty, sunroofChance, chanceBroken: 50);
        car.Suspension = new Suspension(r.Next(1, 3), suspensionDifficulty, suspensionPrice);
        car.Type = ECarType.CITY;
        car.Xenon = new Equipment(Condition.BROKEN, xenonRarityBonus, xenonPrice, xenonDifficulty, xenonChance, chanceBroken: 50);
        car.Year = r.Next(minYear, maxYear);
        SetEngine(1, 3, car);
        SetGearbox(1, 3, car);

        return car;
    }



    /// <summary>
    /// Creates car with random statistics. Usually they are low.
    /// </summary>
    /// <returns></returns>
    public Car GetBarnFind()
    {
        GetCarValues();
        car.Abs = new Equipment(Condition.BROKEN, absRarityBonus, absPrice, absDifficulty, chance: absChance, chanceBroken: 50);
        car.Ac = new Equipment(Condition.BROKEN, acRarityBonus, acPrice, acDifficulty, acChance, chanceBroken: 50);
        car.Airbags = new Equipment(Condition.BROKEN, airbagRarityBonus, airbagPrice, airbagDifficulty, airbagChance, chanceBroken: 50);
        car.BasePrice = baseprice;
        car.Body = new Body(r.Next(1, 3), bodyDifficulty, bodyPrice, true);
        car.CruiseControl = new Equipment(Condition.BROKEN, cruiseRarityBonus, cruisePrice, cruiseDifficulty, cruiseChance, chanceBroken: 50);
        car.Esp = new Equipment(Condition.BROKEN, espRarityBonus, espPrice, espDifficulty, espChance, chanceBroken: 50);
        car.IsBarnFind = true;
        car.IsMileageReal = true;
        car.IsStolen = false;
        car.LeatherSeats = new Equipment(Condition.BROKEN, leatherRarityBonus, leatherPrice, leatherDifficulty, leatherChance, chanceBroken: 50);
        car.Mileage = mileage;
        car.Name = ECarName.Error_Car;
        car.ParkingSensors = new Equipment(Condition.BROKEN, parkingRarityBonus, parkingPrice, parkingDifficulty, parkingChance, chanceBroken: 50);
        car.Radio = new Equipment(Condition.BROKEN, radioRarityBonus, radioPrice, radioDifficulty, radioChance, chanceBroken: 50);
        car.Rarity = rarity;
        car.Realmileage = realMileage;
        car.SunRoof = new Equipment(Condition.BROKEN, sunroofRarityBonus, sunroofPrice, sunroofDifficulty, sunroofChance, chanceBroken: 50);
        car.Suspension = new Suspension(r.Next(1, 3), suspensionDifficulty, suspensionPrice);
        car.Type = ECarType.CITY;
        car.Xenon = new Equipment(Condition.BROKEN, xenonRarityBonus, xenonPrice, xenonDifficulty, xenonChance, chanceBroken: 50);
        car.Year = r.Next(minYear, maxYear);
        SetEngine(1, 3, car);
        SetGearbox(1, 3, car);

        return car;
    }


    protected virtual void SetEngine(int minCondition, int maxCondition, Car _car)
    {
    }

    protected virtual void SetGearbox(int minCondition, int maxCondition, Car _car)
    {
    }

}
