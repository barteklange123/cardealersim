﻿/// <summary>
/// Use this class to get car object. Do not use other classes! 
/// </summary>
public class CarFactory
{
    //Car car = new Car();

    /// <summary>
    /// Method is used to get car object by requesting car's feature.
    /// </summary>
    /// <param name="request">Request is predefined uppercase string with a type of car you want to be returned. Requests: "RANDOM", "RANDOM_STOLEN", "RANDOM_NOT_STOLEN"
    /// "RANDOM_BARNFIND"... (to be completed)</param>
    /// <returns>Returns completed car object</returns>
    public Car GetCar(string request)
    {
        switch (request)
        {
            case "RANDOM":
                return RandomCar.GetCar();
            default:
                ErrorCar errorCar = new ErrorCar();
                return errorCar.GetRandom();    
        }
    }

    public Car GetCar(ECarName carName)
    {
        switch (carName)
        {
            case ECarName.Error_Car:
                ErrorCar errorCar = new ErrorCar();
                return errorCar.GetRandom();
        }

        return null;
    }

    public Car GetCar(ECarType carType)
    {

        return null;
    }

}
