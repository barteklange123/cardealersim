﻿using System;
using UnityEngine;

public enum GearboxType { AUTOMATIC, MANUAL };

[Serializable]
public class Gearbox : CarPart
{
    [SerializeField]
    GearboxType type;
    [SerializeField]
    int rarityBonus;
    [SerializeField]
    int priceBonus;


    /// <summary>
    /// Create gearbox.
    /// </summary>
    /// <param name="condition">Ranges from 1 to 5.</param>
    /// <param name="difficulty">Ranges from 0 to 100.</param>
    /// <param name="type">Automatic or manual. Use GearboxType enum.</param>
    /// <param name="rarityBonus">Bonus to rarity.</param>
    /// <param name="priceBonus">Bonus to price.</param>
    public Gearbox(int condition, int difficulty, GearboxType type, int rarityBonus, int priceBonus) : base(condition, difficulty, priceBonus)
    {
        this.Type = type;
        this.RarityBonus = rarityBonus;
        this.PriceBonus = priceBonus;
    }

    public GearboxType Type
    {
        get
        {
            return type;
        }

        set
        {
            type = value;
        }
    }

    public int RarityBonus
    {
        get
        {
            return rarityBonus;
        }

        set
        {
            if (value >= 0 && value <= 100)
                rarityBonus = value;
            else
            {
                throw new ValueOutOfRangeException("Value must be in range [0...100]");
            }
        }
    }

}
