﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;


//ZROBIĆ SINGLETON I ZROBIĆ REFACTORING
[Serializable]
public sealed class DataStorage
{

    //Data to save:
    [SerializeField]
    List<Car> playerCars;
    [SerializeField]
    string jsonToSave;
    [SerializeField]
    string playerCarsJson;
    //END OF Data to save

    //Paths:
    string saveFileName = "save";
    string saveFolder = "Save";
    //END OF Paths

    //Misc
    //END OF Misc

    public void Save()
    {
        //Array that will contain json-serialized objects.
        string[] saveFile = new string[1];

        //parsing playerCars to Json
        Car[] playerCarsArray = playerCars.ToArray();
        playerCarsJson = JsonHelper.ToJson(playerCarsArray, true);

        saveFile[0] = playerCarsJson;

        jsonToSave = JsonHelper.ToJson(saveFile, true); //TEST

        string path = GetPath(saveFileName + GetTimeStamp() + ".txt");

        //   File.Create(path);
        File.WriteAllText(path, jsonToSave);
    }

    public string LoadFile()
    {
        string saveFileJson;

        DirectoryInfo directoryInfo = new DirectoryInfo(GetPath());
        FileInfo[] fileInfo = directoryInfo.GetFiles("*.txt");
        List<FileInfo> sortedFileList = Sort(fileInfo);


        FileStream fileStream = File.OpenRead(GetPath(sortedFileList[0].Name));
        Debug.Log(sortedFileList[0].Name); //name of loaded file

        StreamReader streamReader = new StreamReader(fileStream);
        saveFileJson = streamReader.ReadToEnd();

        return saveFileJson;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns>Returns path of FILE</returns>
    private string GetPath(string fileName)
    {
#if UNITY_EDITOR
        return Application.dataPath + "/" + saveFolder + "/" + fileName;
#elif UNITY_ANDROID
        return Application.persistentDataPath+"/"+fileName;
#elif UNITY_IPHONE
        return Application.persistentDataPath+"/"+fileName;
#else
        return Application.dataPath +"/"+fileName;
#endif
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns>Returns path of FOLDER</returns>
    private string GetPath()
    {
#if UNITY_EDITOR
        return Application.dataPath + "/" + saveFolder;
#elif UNITY_ANDROID
        return Application.persistentDataPath+"/"+fileName;
#elif UNITY_IPHONE
        return Application.persistentDataPath+"/"+fileName;
#else
        return Application.dataPath +"/"+fileName;
#endif
    }



    /// <summary>
    /// Time stamp is used for creating unique id for save file
    /// </summary>
    /// <returns>Time stamp created of: year, month, day, hour, minute, second</returns>
    private string GetTimeStamp()
    {
        string year = DateTime.UtcNow.Year.ToString();
        string month = DateTime.UtcNow.Month.ToString();
        string day = DateTime.UtcNow.Day.ToString();
        string hour = DateTime.UtcNow.Hour.ToString();
        string minute = DateTime.UtcNow.Minute.ToString();
        string second = DateTime.UtcNow.Second.ToString();

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.Append(year);
        stringBuilder.Append(month);
        stringBuilder.Append(day);
        stringBuilder.Append(hour);
        stringBuilder.Append(minute);
        stringBuilder.Append(second);
        string timeStamp = stringBuilder.ToString();

        return timeStamp;
    }



    /// <summary>
    /// Sorts and changes array into List. Sorts from newest to oldest.
    /// </summary>
    /// <param name="unstortedArray"></param>
    /// <returns>List sorted from newest to oldest.</returns>
    private List<FileInfo> Sort(FileInfo[] unstortedArray)
    {
        List<FileInfo> fileList = new List<FileInfo>();


        foreach (FileInfo file in unstortedArray)
        {
            fileList.Add(file);
        }

        if (fileList.Count > 1)
            fileList.Sort((x, y) => DateTime.Compare(y.CreationTimeUtc, x.CreationTimeUtc));

        return fileList;
    }

    //GETTERS AND SETTERS -------------------------------------------------------------------------------------------------------------------------------------
    public List<Car> PlayerCars
    {
        get
        {
            string saveFileJson = LoadFile();
            string[] saveFileArray = JsonHelper.FromJson<string>(saveFileJson);
            Car[] carArray = JsonHelper.FromJson<Car>(saveFileArray[0]);

            playerCars = new List<Car>();
            foreach (Car car in carArray)
            {
                playerCars.Add(car);
            }

            return playerCars;
        }

        set
        {
            playerCars = value;
        }
    }

}

