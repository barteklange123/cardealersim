﻿public enum EPlayerStats { expertiseLevel, mechanicLevel, charismaLevel, insightLevel}


/// <summary>
/// Class representing Player. Implements Singleton pattern, so get instance by using Instance method.
/// </summary>
public sealed class Player
{
    private static Player oInstance = null; //nie wiem dlaczego przedrostek "o" - skopiowałem to z jakiejś strony
    private int instanceCounter = 0;

    //stats:
    private int expertiseLevel;
    private int mechanicLevel;
    private int charismaLevel;
    private int insightLevel;
    //end of stats

    //Misc:
    public readonly int maxLevel = 5;

    //Singleton implementation
    public static Player Instance
    {
        get
        {
            if (oInstance == null)
            {
                oInstance = new Player();
            }
            return oInstance;
        }
    }

    private Player()
    {
        instanceCounter = 1;
    }
    //End of Singleton implementation

    public void CreateNewPlayer()
    {
        this.ExpertiseLevel = 1;
        this.MechanicLevel = 1;
        this.CharismaLevel = 1;
        this.InsightLevel = 1;
    }


    public int ExpertiseLevel
    {
        get
        {
            return expertiseLevel;
        }

        set
        {
            if (value >= 1 && value <= 5)
            {
                expertiseLevel = value;
            }
                
            else
            {
                throw new ValueOutOfRangeException("Value must be in [1; 5] range");
            }
        }
    }

    public int MechanicLevel
    {
        get
        {
            return mechanicLevel;
        }

        set
        {
            if (value >= 1 && value <= 5)
            {
                mechanicLevel = value;
            }

            else
            {
                throw new ValueOutOfRangeException("Value must be in [1; 5] range");
            }
        }
    }

    public int CharismaLevel
    {
        get
        {
            return charismaLevel;
        }

        set
        {
            if (value >= 1 && value <= 5)
            {
                charismaLevel = value;
            }

            else
            {
                throw new ValueOutOfRangeException("Value must be in [1; 5] range");
            }
        }
    }

    public int InsightLevel
    {
        get
        {
            return insightLevel;
        }

        set
        {
            if (value >= 1 && value <= 5)
            {
                insightLevel = value;
            }

            else
            {
                throw new ValueOutOfRangeException("Value must be in [1; 5] range");
            }
        }
    }



     public void LevelUp(EPlayerStats ePlayerStats) //DOKOŃCZYĆ, ŻEBY KOSZTOWAŁO SZMAL/EXP
    {
        switch (ePlayerStats)
        {
            case EPlayerStats.charismaLevel:
                if (charismaLevel < maxLevel)
                {
                    charismaLevel++;
                }
                break;
            case EPlayerStats.expertiseLevel:
                if (expertiseLevel < maxLevel)
                {
                    expertiseLevel++;
                }
                break;
            case EPlayerStats.insightLevel:
                if (insightLevel < maxLevel)
                {
                    insightLevel++;
                }
                break;
            case EPlayerStats.mechanicLevel:
                if (mechanicLevel < 5)
                {
                    mechanicLevel++;
                }
                break;
        }
    }
}
