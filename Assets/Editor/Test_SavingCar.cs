﻿using NUnit.Framework;
using System.Collections.Generic;

public class Test_SavingCar {
    ErrorCar errorCar = new ErrorCar(); //Car is created in easy "test" way, without using CarFactory
  
    [Test]
	public void Save()
    {
        Car car = errorCar.GetRandom();
        DataStorage dataStorage = new DataStorage();
        List<Car> carList = new List<Car>
        {
            car
        };
        dataStorage.PlayerCars = carList;

        Assert.DoesNotThrow(() => dataStorage.Save());
    }
}
