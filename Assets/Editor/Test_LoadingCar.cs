﻿using NUnit.Framework;
using System.Collections.Generic;
using UnityEngine;

public class Test_LoadingCar
{
    //ErrorCar errorCar = new ErrorCar(); //Car is created in easy "test" way, without using CarFactory

    [Test]
    public void Load()
    {
        DataStorage dataStorage = new DataStorage();

        List<Car> carList = dataStorage.PlayerCars;
        Car car = carList[0];
        Assert.True(car.IsValid());
    }
}
