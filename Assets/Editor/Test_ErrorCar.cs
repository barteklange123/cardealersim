﻿using NUnit.Framework;
using UnityEngine;

public class Test_ErrorCar : CarCreatingTest
{
    ErrorCar errorCar = new ErrorCar();
    Car car = new Car();

    [Test]
    public override void GetBarnFind()
    {
        car = errorCar.GetBarnFind();
        CheckCar(car);
        Assert.True(car.IsValid());
        Assert.True(car.IsBarnFind);
    }

    [Test]
    public override void GetRandom()
    {
        car = errorCar.GetRandom();
        CheckCar(car);
        Assert.True(car.IsValid());
    }

    [Test]
    public override void GetRandomNotStolen()
    {
        car = errorCar.GetRandomNotStolen();
        CheckCar(car);
        Assert.True(car.IsValid());
        Assert.False(car.IsStolen);
    }

    [Test]
    public override void GetRandomStolen()
    {
        car = errorCar.GetRandomStolen();
        CheckCar(car);
        Assert.True(car.IsValid());
        Assert.True(car.IsStolen);
    }

}
